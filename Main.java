package com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sorteio 1");
        Random random = new Random();
        int numero1 = random.nextInt(6 + 1);
        System.out.println(numero1);

        /* Parte 2 */

        System.out.println("Sorteio 2");
        int somaSorteio = 0;
        int sorteado = 0;
        for(int i = 1; i <=3;i++) {
            sorteado = random.nextInt(6 + 1) ;
            System.out.print(sorteado);
            System.out.print(", ");
            somaSorteio += sorteado;
        }
        System.out.println(somaSorteio);

        System.out.println("Sorteio 3");

        for(int x = 1; x <= 3; x++)
        {
            somaSorteio = 0;
            sorteado = 0;
            for(int i = 1; i <=3;i++) {
                sorteado = random.nextInt(6) + 1;
                System.out.print(sorteado);
                System.out.print(", ");
                somaSorteio += sorteado;
            }
            System.out.println(somaSorteio);

        }

    }
}
